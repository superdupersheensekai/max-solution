package com.epam.rd.autotasks.max;

import java.util.OptionalInt;
import java.util.stream.IntStream;

public class MaxMethod {
    public static OptionalInt max(int[] values) {
        if (values == null || values.length == 0) {
            return OptionalInt.empty();
        }
        return OptionalInt.of(IntStream.of(values).reduce(Integer.MIN_VALUE, Integer::max));
    }
}
